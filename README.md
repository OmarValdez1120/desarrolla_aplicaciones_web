**desarrolla apliaciones web**
**Omar Valdez de los Rios 5AVP**

-Practica #1 - 02/09/2022 - Practica de ejemplo
Commit: 8d65ff78437123c0f02980ec4a51db36de9ae13c
Archivo: https://gitlab.com/OmarValdez1120/desarrolla_aplicaciones_web/-/blob/main/parcial1/practica_ejemplo.html

-Practica #2 - 09/09/2022 - Practica JavaScript
Commit: 538690cf67838b26dc6703b5ea2475ca8d437641
Archivo: https://gitlab.com/OmarValdez1120/desarrolla_aplicaciones_web/-/blob/main/parcial1/PracticaJavaScript.html

-Practica #3 - 15/09/2022 - Practica web con bases de datos-parte 1
Commit: 1c3f7f44db3a08f73c571cd4808e02f3ef40b18b
Archivo: https://gitlab.com/OmarValdez1120/desarrolla_aplicaciones_web/-/blob/main/parcial1/PracticaWebDatos.rar

-Practica #4 - 19/09/2022 - Practica web con bases de datos - Vista de consulta de datos
Commit: c8b75da80674a0d80cbc8b4cf18c341f39ff0f3a
Archivo: https://gitlab.com/OmarValdez1120/desarrolla_aplicaciones_web/-/blob/main/parcial1/PracticaWebDatos/consultarDatos.php

-Practica #5 - 19/09/2022 - Practica web con bases de datos - Vista de registro de datos
Commit: f5e6e3945ae80b985a9f87e65975cad6680300b6
Archivo: https://gitlab.com/OmarValdez1120/desarrolla_aplicaciones_web/-/blob/main/parcial1/PracticaWebDatos/registrarDatos.html

-Practica #6 - 23/09/2022 - Practica Web con bases de datos - Mostrar registros en consulta de datos
consultarDatos.php     -     COMMIT:43c91119ed46254cca017a2352ae30e710b10ef0
conexion.php           -     COMMIT:43b5fab0c3fdbe02e1c7e7e0ce0713795b4fe9a3
